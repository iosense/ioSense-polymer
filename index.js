/**
 * ioSense Polymer component
 *
 * @author Jacky Bourgeois
 */

// === === === Read config file === === ===

const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

// Setting the logs
const log4js = require('log4js');
const logger = log4js.getLogger('[test]');
logger.level = config.logger.level;

const express = require('express');
const path = require('path');

const httpsClient = require('https');

const app = express();
const http = require('http').Server(app);
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));

app.use(express.static(path.join(__dirname, '.'),
    {etag: false, maxAge: 100}));

app.post('/:ns/login', function(req, res) {
    let username = req.body.username;
    let password = req.body.password;
    if (username !== undefined && password !== undefined) {
        const params = {
            grant_type: 'password',
            client_id: config.clientId + '_' + req.params.ns,
            client_secret: config.secret,
            username: username + '_' + req.params.ns,
            password: password,
        };

        const body = [];
        for (let p in params) {
            if (params.hasOwnProperty(p)) {
                body.push(encodeURIComponent(p)
                    + '=' + encodeURIComponent(params[p]));
            }
        }
        const strBody = body.join('&');

        send('/' + req.params.ns + '/oauth/token', 'POST', null, strBody,
            'application/x-www-form-urlencoded', function(error, token) {
                if (error) {
                    return res.status(403).json(
                        {error: {code: 0, message: 'Access denied.'}});
                } else {
                    return res.status(200).json(token);
                }
            });
    } else {
        return res.status(403).json(
            {error: {code: 0, message: 'Missing username or password'}});
    }
});


/**
 * Send HTTP requests.
 */
function send(endpoint, method, token, body, contentType, callback) {
    let bodyStr = body;

    if (bodyStr === null) {
        bodyStr = '';
    } else if (typeof body !== 'string' && !(body instanceof String)) {
        bodyStr = JSON.stringify(body);
    }

    const header = {
        'Content-Type': contentType,
        'Content-Length': Buffer.byteLength(bodyStr),
    };

    if (token !== null) {
        header.Authorization = token;
    }

    // An object of options to indicate where to post to
    const options = {
        host: config.iosense.host,
        port: config.iosense.port,
        path: endpoint,
        method: method,
        headers: header,
    };

    // Set up the request
    let message = '';
    const postReq = httpsClient.request(options, function(res) {
        res.setEncoding('utf8');
        res.on('error', function(err) {
            logger.info('error: ' + err);
        });
        res.on('data', function(chunk) {
            message += chunk;
        });
        res.on('end', function(chunk) {
            try {
                const jsonData = JSON.parse(message);
                callback(null, jsonData);
            } catch (err) {
                callback({code: 0, message:
                'Unable to parse the response as JSON for endpoint ' + endpoint
                + '\nResponse:\n' + message,
                });
            }
        });
    });

    // post the data
    postReq.write(bodyStr);
    postReq.end();
}


/**
 * Publish the server.
 */
const message = 'ioSense Developer Portal is listening on port : ';
http.listen(5555, function() {
    logger.info(message + 5555 + ' (http)');
});
